

import java.io.*;
import java.net.*;
import java.util.*;


    public class AuctionServer {

        private static PrintWriter out;

        private ServerSocket serverSocket;

        private Socket clientSocket;

        private static int countOFClient;

        protected static long time;

        protected static String productName;

        private static BufferedReader reader;

        protected static int minCost;

        protected static int maxCost;

        protected static volatile int costFromCustomer;

        private static boolean switcher;

        protected static ArrayList<MultithreadingFoServer> threads = new ArrayList<>();



        public AuctionServer() {

            switcher = true;

        }

        public void start(int port)  {

            try {

                serverSocket = new ServerSocket(port);

                serverSocket.setSoTimeout((int) (time + 10000));

                int customerNumber = 0;

                while (switcher) {

                    clientSocket = serverSocket.accept();

                    out = new PrintWriter(clientSocket.getOutputStream(), true);

                    if (countOFClient > customerNumber) {

                        MultithreadingFoServer multithreading = new MultithreadingFoServer(clientSocket, ++customerNumber);

                        threads.add(multithreading);

                        Thread thread = new Thread(multithreading);

                        thread.start();

                        out.println("Welcome to the auction, please wait for all participants");

                    } else {

                        out.println("Sorry, the connection is failed because exceeded limit of participants");

                        clientSocket.close();

                    }

                    if (countOFClient == customerNumber) {

                        sendMessegeAllCustomeres("The auction starts the product is " + productName + "." + " Bidding starts at a price " + minCost + "$. Do your bides!", -1);

                        for (int i = 0; i < threads.size(); i++) {

                            customerNumber++;

                            ServerHandler.timer();

                        }

                    }

                }

            } catch (IOException e) {

                switcher = false;

                closeConnection();

            }

        }



        public static void sendMessegeAllCustomeres(String message, int exceptOne) {

            for (int i = 0; i < threads.size(); i++) {

                if (i == exceptOne) continue;

                threads.get(i).setMessage(message);

            }

        }

        public static void sendMessege(String message, int number) {

            threads.get(number).setMessage(message);

        }



        public static void main(String[] args) throws IOException, InterruptedException {

            reader = new BufferedReader(new InputStreamReader(System.in));

            System.out.println("How many customers to expect?");

            countOFClient = Integer.parseInt(reader.readLine());

            System.out.println("How long auction will be open?");

            time = Long.parseLong(String.valueOf(reader.readLine()));

            System.out.println("Specify the product that you want to sell?");

            productName = reader.readLine();

            System.out.println("Specify the minimum $ cost of the product?");

            minCost = Integer.parseInt(reader.readLine());

            System.out.println("Specify the minimum $ desired product?");

            maxCost = Integer.parseInt(reader.readLine());

            AuctionServer server = new AuctionServer();

            ServerHandler handler = new ServerHandler();

            Thread thread = new Thread(handler);

            thread.start();

            server.start(5555);

        }



        public void closeConnection() {

            try {

                out.close();

                clientSocket.close();

                for (int i = 0; i < threads.size(); i++) {

                    threads.get(i).closeConnection();

                }

                serverSocket.close();

            } catch (IOException e) {

                e.printStackTrace();

            }

        }

    }



    class ServerHandler implements Runnable {

        private static boolean timeSwitcher = true;

        @Override

        public void run() {

            while (true) {

                if (AuctionServer.costFromCustomer >= AuctionServer.maxCost) { // Winner was found

                    int winner = findTheWinner();

                    sendMessageAllCustomers("The product has been sold to the customer with the number " + winner +

                            " for the price " + AuctionServer.costFromCustomer + "$", winner - 1, false);

                    break;

                }

                if (!timeSwitcher) { // time out

                    sendMessageAllCustomers("We apologize, but rates are postponed to next Sunday for technical reasons.", -1, false);

                    break;

                }

            }

            try {

                Thread.sleep(500);

            } catch (InterruptedException e) {

                e.printStackTrace();

            }

            sendMessageAllCustomers("The auction is completed.", -1, false);

            MultithreadingFoServer.setSwitcher(false);

        }



        public void sendMessageAllCustomers(String message, int number, boolean oneOrExceptOne) {

            if (oneOrExceptOne && number > -1) AuctionServer.threads.get(number).sendMessage(message); //send one

            else if (!(oneOrExceptOne) && !(number == -1)) AuctionServer.sendMessegeAllCustomeres(message, number); //send all except one

            else AuctionServer.sendMessegeAllCustomeres(message, -1); //send all

        }

        public int findTheWinner() {

            for (int i = 0; i < AuctionServer.threads.size(); i++) {

                if (AuctionServer.threads.get(i).getMyPrice() == AuctionServer.costFromCustomer) {

                    sendMessageAllCustomers("Thank you for your purchase " + AuctionServer.threads.get(i).getName(), i, true); //the message send to winner

                    return AuctionServer.threads.get(i).getClientNumber();

                }

            }

            return 0;

        }

        public static void setTime(boolean time) {

            ServerHandler.timeSwitcher = time;

        }

        public static void timer() {

            Timer timer = new Timer();

            timer.schedule(new TimerTask1(), AuctionServer.time);

        }

        static class TimerTask1 extends TimerTask {

            @Override

            public void run() {

                ServerHandler.setTime(false);

            }

        }

    }



    class MultithreadingFoServer implements Runnable {

        private String name;

        private PrintWriter out;

        private BufferedReader in;

        private Socket client;

        private int clientNumber;

        private int myPrice;

        private static boolean switcher;

        private volatile String message = "";



        public MultithreadingFoServer(Socket client, int clientNumber) {

            this.client = client;

            this.clientNumber = clientNumber;

            switcher = true;

        }


        public String getName() {

            return name;

        }


        public static void setSwitcher(boolean switcher) {

            MultithreadingFoServer.switcher = switcher;

        }

        public int getClientNumber() {

            return clientNumber;

        }

        public int getMyPrice() {

            return myPrice;

        }

        public void sendMessage(String message) {

            out.println(message);

            this.message = "";

        }

        @Override

        public void run() {



            try {

                out = new PrintWriter(client.getOutputStream(), true);

                in = new BufferedReader(new InputStreamReader(client.getInputStream()));

                name = in.readLine();

                System.out.println(name);

            } catch (IOException e) {

                e.printStackTrace();

            }

            while (switcher) {

                if (!message.isEmpty()) sendMessage(message);

                try {

                    if (in.ready()) {

                        String msg = in.readLine();

                        System.out.println(msg);

                        myPrice = Integer.parseInt(msg);

                        if (myPrice < AuctionServer.minCost) throw new NumberFormatException();

                        AuctionServer.costFromCustomer = myPrice;

                        AuctionServer.sendMessegeAllCustomeres("Participant with number " + clientNumber +  " do rate " + msg + "$", -1);

                    }

                } catch (IOException | NumberFormatException e) {

                    AuctionServer.sendMessege("You have offered not the correct price, or the price is lower than the starting price. Please repeat the bid.", clientNumber - 1);

                    continue;

                }

            }

        }

        public void closeConnection() {

            try {

                in.close();

                out.close();

                client.close();

            } catch (IOException e) {

                e.printStackTrace();

            }

        }

        public void setMessage(String message) {

            this.message = message;

        }

    }

