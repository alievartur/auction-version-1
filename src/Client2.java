
    import java.io.*;
    import java.net.*;


public class Client2 {
    private static boolean permissionToBid;

    private Socket clientSocket;

    private PrintWriter out;

    private static BufferedReader in;

    private static BufferedReader reader;

    private static boolean switcher = true;



    public void startConnection(String ip, int port) throws IOException {

        clientSocket = new Socket(ip, port);

        out = new PrintWriter(clientSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        reader = new BufferedReader(new InputStreamReader(System.in));

    }

    public String sendAndGetMessage(String msg) throws IOException, InterruptedException {

        out.println(msg);

        out.flush();

        String resp = in.readLine();

        return resp;

    }

    public void send(String msg) throws IOException, InterruptedException {

        out.println(msg);

        out.flush();

    }

    public static void readMessage() throws IOException {

        while (in.ready()) {

            String msg = in.readLine();

            System.out.println(msg);

            if (msg.contains("Bidding starts")) permissionToBid = true;

            else if (msg.contains("The auction is completed") && msg.contains("Thank you for your purchase")) {

                permissionToBid = false;

                switcher = false;

                break;

            }

        }

    }

    public void stopConnection() throws IOException {

        in.close();

        out.close();

        clientSocket.close();

    }

    public static void main(String[] args) throws IOException, InterruptedException {

        Client2 client = new Client2();

        client.startConnection("127.0.0.1", 5555);

        System.out.println("Please write your full name");

        String name = reader.readLine();

        client.send(name);

        System.out.println(in.readLine());

        while ( switcher ) {

            if (in.ready()) readMessage();

            if (permissionToBid) {

                String line = reader.readLine();

                String response = client.sendAndGetMessage(line);

                System.out.println(response);

            }

        }

        client.stopConnection();

    }

}

